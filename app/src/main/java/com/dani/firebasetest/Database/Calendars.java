package com.dani.firebasetest.Database;

public class Calendars {

    private Integer durationInMinutes;
    private Integer repeatIntervalInDays;
    private String startTime;

    public Integer getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(Integer durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    public Integer getRepeatIntervalInDays() {
        return repeatIntervalInDays;
    }

    public void setRepeatIntervalInDays(Integer repeatIntervalInDays) {
        this.repeatIntervalInDays = repeatIntervalInDays;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @Override
    public String toString() {
        return "Calendars{" +
                "durationInMinutes='" + durationInMinutes + '\'' +
                ", repeatIntervalInDays='" + repeatIntervalInDays + '\'' +
                ", startTime='" + startTime + '\'' +
                '}';
    }
}
