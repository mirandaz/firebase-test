package com.dani.firebasetest.Database;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CalendarsDAO {

    private static final String COLLECTION_NAME = "calendars";
    private static final FirebaseFirestore DB = FirebaseFirestore.getInstance();
    private static final String TAG = CalendarsDAO.class.getName();

    public static void insertRecord(Calendars calendars) {
        DB.collection(COLLECTION_NAME).add(calendars);
    }

    public static Map<String, Calendars> getAllRecords() {
        Map<String, Calendars> calendarsList = new HashMap<>();

        Task<QuerySnapshot> calendarsTask = DB.collection(COLLECTION_NAME).get();
        while (!calendarsTask.isComplete()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (calendarsTask.isSuccessful()) {
            for (QueryDocumentSnapshot document : Objects.requireNonNull(calendarsTask.getResult())) {
                calendarsList.put(document.getId(),parseResult(document.getData()));
            }
        } else {
            Log.w(TAG, "Error getting documents.", calendarsTask.getException());
        }
        return calendarsList;
    }

    private static Calendars parseResult(Map<String, Object> map) {
        Calendars calendars = new Calendars();

        calendars.setDurationInMinutes(Integer.parseInt(String.valueOf(map.get("durationInMinutes"))));
        calendars.setRepeatIntervalInDays(Integer.parseInt(String.valueOf(map.get("repeatIntervalInDays"))));
        calendars.setStartTime(String.valueOf(map.get("startTime")));

        return calendars;
    }

    public static Calendars getNewCalendar(
            Integer durationInMinutes,
            Integer repeatIntervalInDays,
            String startTime
    ) {
        Calendars newConfig = new Calendars();
        newConfig.setDurationInMinutes(durationInMinutes);
        newConfig.setRepeatIntervalInDays(repeatIntervalInDays);
        newConfig.setStartTime(startTime);

        return newConfig;
    }
}
