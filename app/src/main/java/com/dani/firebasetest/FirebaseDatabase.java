package com.dani.firebasetest;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class FirebaseDatabase {

    private static final FirebaseFirestore DB = FirebaseFirestore.getInstance();


    public static void insertNewDeviceToken(String token){

        Map<String, String> newDevice = new HashMap<>();
        newDevice.put("deviceName", Utilities.getDeviceName());
        newDevice.put("token", token);

        DB.collection("registeredDevices").add(newDevice);
    }

}
